/*
    This code by platy11 based on code from
    Quark by Vazkii and contributors
    
    Licensed under the CC-BY-NC-SA 3.0 license
    https://creativecommons.org/licenses/by-nc-sa/3.0/deed.en_GB
*/

package io.github.platy11.swvpackhelper;

import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

import com.google.common.collect.ImmutableSet;
import com.mojang.authlib.minecraft.MinecraftProfileTexture.Type;
import org.apache.logging.log4j.Logger;

import net.minecraft.client.entity.AbstractClientPlayer;
import net.minecraft.client.network.NetworkPlayerInfo;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
// import net.minecraft.init.Blocks;

import net.minecraftforge.client.event.RenderPlayerEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.ReflectionHelper;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;


@Mod(modid = SWVPackHelper.MODID, name = SWVPackHelper.NAME, version = SWVPackHelper.VERSION)
public class SWVPackHelper
{
    public static final String MODID = "swvpackhelper";
    public static final String NAME = "SWV Modpack Helper";
    public static final String VERSION = "1.0";
    
//    @SidedProxy(clientSide = "io.github.platy11.swvpackhelper.clientproxy", serverSide = "io.github.platy11.swvpackhelper.commonproxy")
//    public static CommonProxy proxy;
//    
//    public class ClientProxy extends CommonProxy {}
//
//    private static Logger logger;

    @EventHandler
    public void preInit(FMLPreInitializationEvent event)
    {
        // logger = event.getModLog();
    }

    @EventHandler
    public void init(FMLInitializationEvent event)
    {
        MinecraftForge.EVENT_BUS.register(CapeHandler.class);
    }    
    
}

class CapeHandler {
    private static final ImmutableSet<String> UUIDS = ImmutableSet.of("16d2f32a-602f-45cf-8445-df2ad20fe883");

	private static final Set<EntityPlayer> done = Collections.newSetFromMap(new WeakHashMap());
    
    private static final String[] PLAYER_INFO = new String[] { "d", "field_175157_a", "playerInfo" };
    private static final String[] PLAYER_TEXTURES = new String[] { "a", "field_187107_a", "playerTextures" };
    
    @SubscribeEvent
	public static void onRenderPlayer(RenderPlayerEvent.Post event) {
		EntityPlayer player = event.getEntityPlayer();
		String uuid = player.getUUID(player.getGameProfile()).toString();
		if(player instanceof AbstractClientPlayer && UUIDS.contains(uuid) && !done.contains(player)) {
			AbstractClientPlayer clplayer = (AbstractClientPlayer) player;
			if(clplayer.hasPlayerInfo()) {
				NetworkPlayerInfo info = ReflectionHelper.getPrivateValue(AbstractClientPlayer.class, clplayer, PLAYER_INFO);
				Map<Type, ResourceLocation> textures = ReflectionHelper.getPrivateValue(NetworkPlayerInfo.class, info, PLAYER_TEXTURES);
				ResourceLocation loc = new ResourceLocation("swvhelper", "textures/cape.png");
				textures.put(Type.CAPE, loc);
				textures.put(Type.ELYTRA, loc);
				done.add(player);
			}
		}
	}
}
